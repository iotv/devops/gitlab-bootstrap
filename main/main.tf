terraform {
  backend "s3" {}
}

provider "aws" {
  alias = "default"
}

module "backend" {
  source = "../gitlab"

  org_name = "iotv"
  pgp_key  = "keybase:davidjfelix"

  providers = {
    aws = "aws.default"
  }
}
