# Organization name
variable "org_name" {
  description = "The name of your organization to use for this terraform module"
  type        = "string"
}

# PGP Key
variable "pgp_key" {
  description = "The PGP public key to use when encrypting the secret back"
  type        = "string"
}

# IAM user for gitlab to use for s3 deploys
resource "aws_iam_user" "gitlab_main_site_s3" {
  force_destroy = true
  name          = "${var.org_name}-gitlab-s3-main-site"
  path          = "/bastion/"
}
